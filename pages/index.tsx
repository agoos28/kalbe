import React from "react";

import { Container, Header, Main, Footer, Cards } from "@components";
import {serverSideTranslations} from "next-i18next/serverSideTranslations";
import HeroSlider, {SliderProps} from "@components/sections/heroSlider";
import slide1 from "@public/images/homeSlide1.jpg";
import coronavirus from '@public/images/coronavirus.svg';
import lightbulb from '@public/images/lightbulb.svg';
import people from '@public/images/people.svg';
import leaf from '@public/images/leaf.svg';
import explore1 from '@public/images/explore1.jpg';
import homeSlideAlt from '@public/images/homeSlideAlt.jpg';
import slide1Image from '@public/images/drugs1.svg';
import homeSpacer1 from '@public/images/homeSpacer1.png';
import carousel1 from '@public/images/carousel1.jpg';
import carousel2 from '@public/images/carousel2.jpg';
import carousel3 from '@public/images/carousel3.jpg';
import carousel4 from '@public/images/carousel4.jpg';
import work1 from '@public/images/work1.png';
import work2 from '@public/images/work2.png';
import work3 from '@public/images/work3.png';
import work4 from '@public/images/work4.png';
import news1 from '@public/images/news1.jpg';
import HeroOverview, {OverviewProps} from "@components/sections/heroOverview";
import Image from "next/image";
import HeroExplore, {ExploreProps} from "@components/sections/heroExplore";
import HeroMasonry, {MasonryItem, MasonryProps} from "@components/sections/heroMasonry";
import HeroCarousel, {CarouselProps} from "@components/sections/heroCarousel";
import HeroNews, {NewsProps} from "@components/sections/heroNews";
import HeroCare, {CareProps} from "@components/sections/heroCare";
import HeroInnovation, {InnovationProps} from "@components/sections/heroInnovation";
import HeroRelation, {RelationProps} from "@components/sections/heroRelation";
import HeroJoin, {JoinProps} from "@components/sections/heroJoin";
import HeroMap from "@components/sections/heroMap";


const homeSlider: SliderProps['content'] = [{
  title: 'Moving Forward in Pharmacy Industry',
  description: 'Continous Development in The Industry',
  backgroundImage: homeSlideAlt,
  link: '#'
}, {
  title: 'Strive Through Innovation',
  description: 'a provider of an integrated healthcare solution',
  image: slide1Image,
  backgroundImage: slide1,
  link: '#'
}, {
  title: 'Building a Healthy Nation',
  backgroundImage: slide1
}]

const homePageOverview: OverviewProps = {
  title: 'Together, [br][hl]Building[/hl] a Healthy Nation',
  description: 'The story of Kalbe Farma begins from a humble beginning in the founder\'s garage in 1966[br][br] as a pharmaceutical company driven by a set of simple principles: [br][hl]innovation, strong brands and excellent management.[/hl]',
  items: [{
    title: 'Countering [br]The Pandemic',
    iconImage: coronavirus
  }, {
    title: 'R&D [br]Innovation',
    iconImage: lightbulb
  }, {
    title: 'People [br]Employee',
    iconImage: people
  }, {
    title: 'Green [br]Movement',
    iconImage: leaf
  }]
}

const homePageExplore: ExploreProps = {
  title: 'OUR WORK',
  items: [{
    title: 'Countering [br]The Pandemic',
    image: work1
  }, {
    title: 'R&D [br]Innovation',
    image: work2
  }, {
    title: 'People [br]Employee',
    image: work3
  }, {
    title: 'Green [br]Movement',
    image: work4
  }]
}

const homePageMasonry: MasonryProps = {
  title: 'OUR [hl]AREA[/hl] OF FOCUS',
  description: 'KALBE focus in specific area of the business, we encouraged to enhanced our innovation in our product',
  items: [{
    title: "Photo title",
    link: '#',
    image: carousel1
  }, {
    title: "Photo title",
    link: '#',
    image: carousel2
  }, {
    title: "Photo title",
    link: '#',
    image: carousel3
  }, {
    title: "Photo title",
    link: '#',
    image: carousel4
  }, {
    title: "Photo title",
    link: '#',
    image: carousel1
  }, {
    title: "Photo title",
    link: '#',
    image: carousel2
  }, {
    title: "Photo title",
    link: '#',
    image: carousel3
  }, {
    title: "Photo title",
    link: '#',
    image: carousel4
  }]
};

const homePageCare: CareProps = {
  title: 'SUSTAINABILITY',
  description: 'Economic, social and environmental sustainability has become a major concern throughout the world nowadays. United Nations, Governments and private sectors continue to work together to implement sustainability initiatives that aim to contribute as much as possible to the environment, society and the economy for the sake of our stakeholder s future.',
  link: '#'
}

const homePageCarousel: CarouselProps = {
  label: 'CULTURES',
  title: 'GROW WITH US',
  description: 'Our Unique Culture in the industry is one of our advantages to grow our value',
  items: [{
    title: "Photo title",
    link: '#',
    image: carousel1
  }, {
    title: "Photo title",
    link: '#',
    image: carousel2
  }, {
    title: "Photo title",
    link: '#',
    image: carousel3
  }, {
    title: "Photo title",
    link: '#',
    image: carousel4
  }, {
    title: "Photo title",
    link: '#',
    image: carousel1
  }, {
    title: "Photo title",
    link: '#',
    image: carousel2
  }, {
    title: "Photo title",
    link: '#',
    image: carousel3
  }, {
    title: "Photo title",
    link: '#',
    image: carousel4
  }]
}

const homePageInnovation: InnovationProps = {
  title: 'Strive Through Innovation',
  description: 'a provider of an integrated healthcare solution, enhanced innovation during our R&D for a better heatlh.',
  link: '#'
}

const homePageRelation: RelationProps = {
  title: 'TRANSPARENCY & COMMUNICATION',
  description: 'as a public company, Kalbe recognizes the importance of the Company’s relationship with the investors. Therefore, the Company strives to enhance transparency and communication; by providing latest information related to the Company’s business performance and future outlook, to assist investors in making investment decisions in the shares of the Company.',
  link: '#'
}

const homePageJoin: JoinProps = {
  title: 'BE A PART OF US',
  description: 'Our Unique Culture in the industry is one of our advantages to grow our value',
  items: [{
    title: 'EMPLOYEE',
    description: 'Join the future challanging carreer with Kalbe, our main goal to accelerate Human Resource with the need of Pharmacy',
    image: '/images/join1.png',
    link: '#'
  }, {
    title: 'BUSINESS PARTNER',
    description: 'The growth of Industry 5.0 invited the business to growth.',
    image: '/images/join1.png',
    link: '#'
  }]
}

const homePageNews: NewsProps = {
  items: [{
    title: 'REACH INDI 4.0 AWARD, KALBE PROVE DIGITAL TRANSFORMATION ON ALL LINES',
    image: news1
  }, {
    title: 'IDI AND KALBE ANNOUNCE THE WINNER ANUGERAH KARYA CIPTA DOKTER INDONESIA 2021',
    image: news1
  }, {
    title: 'ACCELERATING COVID-19 VACCINATION IN SORONG – WEST PAPUA',
    image: news1
  }]
}

const Home = () => {
  return (
    <Container>
      <Header />
      <HeroSlider content={homeSlider} />
      <div className="relative z-10 pointer-events-none">
        <div className="absolute -translate-y-1/2">
          <Image layout={'intrinsic'} src={homeSpacer1}/>
        </div>
      </div>
      <HeroOverview {...homePageOverview} />
      <HeroExplore {...homePageExplore} />
      <HeroMasonry {...homePageMasonry} />
      <HeroCare {...homePageCare} />
      <HeroCarousel {...homePageCarousel} />
      <HeroInnovation {...homePageInnovation} />
      <HeroRelation {...homePageRelation} />
      <HeroJoin {...homePageJoin} />
      <HeroNews {...homePageNews} />
      <HeroMap />
      <Footer />
    </Container>
  );
};

export const getServerSideProps = async ({ locale }: any) => ({
  props: {
    ...(await serverSideTranslations(locale, ['common']))
  }
});

export default Home;
