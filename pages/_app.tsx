import React from "react";
import { AppProps } from "next/app";
import "tailwindcss/tailwind.css";
import { StyledThemeProvider } from "@definitions/styled-components";
import { Provider } from "react-redux";
import store from "@redux/store";
import {appWithTranslation} from 'next-i18next';
import GlobalStyle from '@styles/globalStyles';


function MyApp(props: AppProps): JSX.Element {
  const {Component, pageProps} = props;
  return (
    <StyledThemeProvider>
      <GlobalStyle />
      <Provider store={store}>
        <Component {...pageProps} />
      </Provider>
    </StyledThemeProvider>
  );
}


export default appWithTranslation(
  MyApp
);
