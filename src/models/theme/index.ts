export interface SimplePaletteColorOptions {
  lighter?: string;
  light?: string;
  main: string;
  dark?: string;
  darker?: string;
  contrastText?: string;
}

export interface ThemeColor {
  primary: SimplePaletteColorOptions;
  secondary: SimplePaletteColorOptions;
  tertiary: SimplePaletteColorOptions;
  error: SimplePaletteColorOptions;
  warning: SimplePaletteColorOptions;
  info: SimplePaletteColorOptions;
  success: SimplePaletteColorOptions;
  textBlack: SimplePaletteColorOptions;
  textWhite: SimplePaletteColorOptions;
  paper: SimplePaletteColorOptions;
  disabled: SimplePaletteColorOptions;
  divider: SimplePaletteColorOptions;
  border: SimplePaletteColorOptions;
  transparent: SimplePaletteColorOptions;
}

export interface ThemeFontFamily {
  fontFamily: string;
  fontWeight?: Pick<'normal' | 'bold' | '100' | '200' | '300' | '400' | '500' | '600', number> ;
}

export interface ThemeFontStyle extends ThemeFontFamily {
  fontSize: number;
  lineHeight: number;
  color?: string;
}

export interface ThemeFont {
  size: {
    giant: number;
    huge: number;
    xxl: number;
    xl: number;
    lg: number;
    ml: number;
    md: number;
    sm: number;
    xs: number;
    xxs: number;
  };
  style: {
    headline: ThemeFontStyle;
    h1: ThemeFontStyle;
    h2: ThemeFontStyle;
    h3: ThemeFontStyle;
    h4: ThemeFontStyle;
    h5: ThemeFontStyle;
    h6: ThemeFontStyle;
    h7: ThemeFontStyle;
    large: ThemeFontStyle;
    normal: ThemeFontStyle;
    small: ThemeFontStyle;
    tiny: ThemeFontStyle;
  };
  family: {
    bold: ThemeFontFamily;
    medium: ThemeFontFamily;
    regular: ThemeFontFamily;
    light: ThemeFontFamily;
  };
}

export type SpacingType = 'xxs' | 'xs' | 'sm' | 'md' | 'ml' | 'lg' | 'xl' | 'xxl' | 'xxxl' | 'xxxxl';