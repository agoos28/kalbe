import moment, {Duration, Moment} from 'moment';
export namespace Utils {
  export function dateTimeFormatter(date: string | number | Date, format?: string): string {
    if (!date) {
      return '';
    }
    return moment(date).format(format || 'DD/MM/YYYY hh:mm');
  }
}