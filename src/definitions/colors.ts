import {ThemeColor} from "../models/theme";

export const colors: ThemeColor = {
  primary: {light: '#ABCA4B', main: '#8DBB00', dark: '#8DBB00'},
  secondary: {light: '#076559', main: '#01443C', dark: '#01312a'},
  tertiary: {light: '#0aa1ff', main: '#0d82cb', dark: '#056DAC'},
  error: {light: '#ffe7e7', main: '#FF3B30', dark: '#cc000f'},
  warning: {light: '#fff9e1', main: '#ffdc00', dark: '#e0b100', darker: '#cb9300'},
  success: {light: '#f0ffe5', main: '#4BD863', dark: '#197d00'},
  info: {light: '#e8f4fd', main: '#007AFF', dark: '#4a90e2'},
  paper: {lighter: 'rgba(255,255,255,0.2)', light: '#FBF8F5', main: '#ffffff', dark: '#F5EEE6', darker: '#EBDCC9'},
  disabled: {lighter: '#F9F9F9', light: '#ebebeb', main: '#ebebeb', dark: '#00000030'},
  textBlack: {lighter: '#F1F1F1', light: '#A5A6AA', main: '#585858', dark: '#000000'},
  textWhite: {light: '#ffffff', main: '#ffffff', dark: '#ffffff7d'},
  divider: {light: '#00000014', main: '#F8F4F0', dark: '#00000014'},
  border: {lighter: '#F1F1F1', light: '#DDDDDD', main: 'rgba(0,0,0,0.30)', dark: 'rgba(0,0,0,0.35)'},
  transparent: {lighter: 'rgba(0,0,0,0.03)', light: 'rgba(0,0,0,0.06)', main: '#00000000', dark: 'rgba(57,60,67,0.40)', darker: 'rgba(57, 60, 67, 0.7)'}
};