import {ThemeFont} from "../models/theme";

const r = 1;

const size = {
  giant: 30,
  huge: 26,
  xxl: 24,
  xl: 20,
  lg: 18,
  ml: 16,
  md: 14,
  sm: 12,
  xs: 10,
  xxs: 8
};

const family = {
  bold: {
    fontFamily: 'Helvetica',
    fontWeight: '600',
  },
  medium: {
    fontFamily: 'Helvetica',
    fontWeight: '500',
  },
  regular: {
    fontFamily: 'Helvetica',
    fontWeight: '400',
  },
  light: {
    fontFamily: 'Helvetica',
    fontWeight: '300',
  },
};

const style = {
  headline: {
    ...family.bold,
    fontSize: Math.round(r * size.huge),
    lineHeight: Math.round(r * 33)
  },
  h1: {
    ...family.bold,
    fontSize: Math.round(r * size.xxl),
    lineHeight: Math.round(r * 31)
  },
  h2: {
    ...family.bold,
    fontSize: Math.round(r * size.xl),
    lineHeight: Math.round(r * 25)
  },
  h3: {
    ...family.bold,
    fontSize: Math.round(r * size.lg),
    lineHeight: Math.round(r * 23)
  },
  h4: {
    ...family.bold,
    fontSize: Math.round(r * size.ml),
    lineHeight: Math.round(r * 20)
  },
  h5: {
    ...family.bold,
    fontSize: Math.round(r * size.md),
    lineHeight: Math.round(r * 18)
  },
  h6: {
    ...family.bold,
    fontSize: Math.round(r * size.sm),
    lineHeight: Math.round(r * 17)
  },
  h7: {
    ...family.bold,
    fontSize: Math.round(r * size.xs),
    lineHeight: Math.round(r * 13)
  },
  large: {
    ...family.regular,
    fontSize: Math.round(r * size.ml),
    lineHeight: Math.round(r * 20)
  },
  normal: {
    ...family.regular,
    fontSize: Math.round(r * size.md),
    lineHeight: Math.round(r * 18)
  },
  small: {
    ...family.regular,
    fontSize: Math.round(r * size.sm),
    lineHeight: Math.round(r * 17)
  },
  tiny: {
    ...family.regular,
    fontSize: Math.round(r * size.xs),
    lineHeight: Math.round(r * 13)
  }
};

export const fonts: ThemeFont = {
  size,
  style,
  family
};
