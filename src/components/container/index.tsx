import * as React from "react";

export const Container = ({children}: any) => {
  return <div className="min-h-screen flex flex-col">
    {children}
  </div>;
};
