import React, {Fragment} from "react";
import Image, {ImageProps} from 'next/image';
import square from '@public/images/square.png';
import 'react-awesome-slider/dist/styles.css';
import 'react-awesome-slider/dist/custom-animations/scale-out-animation.css';
import Text, {TextProps} from "@components/typography";
import {colors} from "@definitions/colors";
import bgOverviewBottom from "@public/images/bgOverviewBottom.png";
// @ts-ignore
import Tilt from 'react-vanilla-tilt';


export interface OverviewItem {
  title: string;
  link?: string;
  iconImage: ImageProps['src'];
}

export interface OverviewProps {
  title: string;
  description: string;
  items: Array<OverviewItem>
}

export interface State {
  activeSlide: number;
}

export default class HeroOverview extends React.Component<OverviewProps, State> {
  parseText = (text: string) => {
    return text.replace(/\[br\]/g, '<br/>')
      .replace(/\[hl\]/g,`<span style="color: ${colors.primary.main}">`)
      .replace(/\[\/hl\]/g, '</span>');
  }

  clearText = (text: string) => {
    return text.replace(/\[br\]/g, '')
      .replace(/\[hl\]/g,``)
      .replace(/\[\/hl\]/g, '');
  }

  renderTitle = () => {
    const {title} = this.props;
    return (
      <Text asDiv={true} className={'lg:text-5xl sm:text-3xl text-2xl lg:mb-8 mb-3'} shade={'dark'}>
        <div style={{lineHeight: 1.2}} dangerouslySetInnerHTML={{ __html: this.parseText(title)}}/>
      </Text>
    );
  }

  renderDesc = () => {
    const {description} = this.props;
    return (
      <Text asDiv={true} type={'normal'} className={'md:text-lg text-md md:text-lg'}>
        <div className="hidden md:block" style={{lineHeight: 1.3}} dangerouslySetInnerHTML={{ __html: this.parseText(description)}}/>
        <span className="md:hidden">{this.clearText(description)}</span>
      </Text>
    );
  }

  renderItems = () => {
    const {items} = this.props;
    const elems: Array<React.ReactElement> = [];
    items.map((item, index) => {
      elems.push(
        <Tilt
          key={index}
          options={{ scale: 40, max: 200, speed: 600, glare: true }}
          className="group relative w-full"
          style={styles.tiltBox}
        >
        <a
          href={item.link}
          className=""
        >
          <div style={styles.background} className="group-hover:brightness-90 transition-all duration-300">
            <Image src={square} layout="responsive" />
          </div>
          <div className="absolute bg-white rounded-full blur-sm" style={{width: 60, height: 60, bottom: '20%', right: '10%'}} />
          <div className="absolute" style={styles.backdrop}>
            <div className="w-8/12 text-center -translate-y-1/4 group-hover:-translate-y-0 transition-all group-hover:color:text-white group-hover:ease-out ease-in">
              <Image
                layout={'intrinsic'}
                src={item.iconImage}
                className=""
                height="100%"
              />
            </div>
          </div>
          <div className="absolute uppercase bottom-5 left-5 text-white lg:text-lg md:text-sm group-hover:text-textWhite-main transition-all duration-700">
            <div style={{lineHeight: 1.3}} dangerouslySetInnerHTML={{ __html: this.parseText(item.title)}}/>
          </div>
        </a>
        </Tilt>
      );
    });

    return (
      <div className="grid grid-cols-2 gap-10 sm:gap-8 lg:gap-12 md:gap-10">
        {elems}
      </div>
    );
  }

  render() {
    return (
      <div
        className="relative flex items-center pb-32 lg:pb-20 overflow-hidden"
        style={{backgroundImage: 'url(/images/bgOverview.jpg)', backgroundPosition: '50% 50%', backgroundRepeat: 'no-repeat', backgroundSize: 'cover'}}
      >
        <div className="container h-auto px-10 md:px-5 lg:px-20 relative mx-auto py-20">
          <div className="grid grid-cols-1 md:grid-cols-2 md:gap-24 gap-12">
            <div>
              <Text asDiv={true} color={colors.primary.main} className={'text-xs md:text-lg font-light mb-3'}>OVERVIEW</Text>
              {this.renderTitle()}
              {this.renderDesc()}
            </div>
            <div>
              {this.renderItems()}
            </div>
            </div>
        </div>
        <div className="absolute bottom-0 z-10 pointer-events-none left-0 right-0 h-32">
          <Image layout={'fill'} objectFit={'fill'} src={bgOverviewBottom}/>
        </div>
      </div>
    );
  }
};

const styles = {
  tiltBox: {
    background: 'none',
    borderRadius: 32,
  },
  background: {
    borderRadius: 32,
    background: 'linear-gradient(159.63deg, rgba(201, 226, 124, 0.7) 6.66%, rgba(136, 181, 0, 0.7) 86.46%)',
  },
  backdrop: {
    pointerEvents: 'none' as any,
    height: '50%',
    width: '90%',
    borderRadius: 32,
    top: '20%',
    left: '30%',
    border: '3px solid rgba(255, 255, 255, 0.3)',
    backdropFilter: 'blur(15px)',
    background: 'linear-gradient(110.62deg, rgba(255, 255, 255, 0.25) 6.39%, rgba(255, 255, 255, 0.065) 53.34%, rgba(255, 255, 255, 0.1025) 53.34%)',
  }
}
