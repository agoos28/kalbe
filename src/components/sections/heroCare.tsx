import React, {Fragment} from "react";
import Image, {ImageProps} from 'next/image';
import mountain from '@public/images/mountain1.png';
import 'react-awesome-slider/dist/styles.css';
import 'react-awesome-slider/dist/custom-animations/scale-out-animation.css';
import Text, {TextProps} from "@components/typography";
import {colors} from "@definitions/colors";
import bgOverviewBottom from "@public/images/bgOverviewBottom.png";
// @ts-ignore
import Tilt from 'react-vanilla-tilt';




export interface CareProps {
  title: string;
  description: string;
  link: string;
}

export default class HeroCare extends React.Component<CareProps> {
  parseText = (text: string) => {
    return text.replace(/\[br\]/g, '<br/>')
      .replace(/\[hl\]/g,`<span style="color: ${colors.primary.main}">`)
      .replace(/\[\/hl\]/g, '</span>');
  }

  clearText = (text: string) => {
    return text.replace(/\[br\]/g, '')
      .replace(/\[hl\]/g,``)
      .replace(/\[\/hl\]/g, '');
  }

  renderTitle = () => {
    const {title} = this.props;
    return (
      <Text asDiv={true} className={'lg:text-5xl sm:text-3xl text-2xl lg:mb-8 mb-3'} shade={'dark'}>
        <div style={{lineHeight: 1.2}} dangerouslySetInnerHTML={{ __html: this.parseText(title)}}/>
      </Text>
    );
  }

  renderDesc = () => {
    const {description} = this.props;
    return (
      <Text asDiv={true} type={'normal'} className={'md:text-lg text-md md:text-lg'}>
        <div className="hidden md:block" style={{lineHeight: 1.3}} dangerouslySetInnerHTML={{ __html: this.parseText(description)}}/>
        <span className="md:hidden">{this.clearText(description)}</span>
      </Text>
    );
  }



  render() {
    return (
      <div
        className="relative flex items-center"
        style={{
          marginBottom: 60,
          backgroundColor: colors.primary.main,
          backgroundImage: 'url(/images/careBg.png)',
          backgroundPosition: '100% 0%',
          backgroundRepeat: 'no-repeat',
        }}
      >
        <div className="container h-auto px-10 md:px-5 lg:px-20 relative mx-auto py-20">
          <Text asDiv={true} color={colors.textWhite.main} className={'text-xs text-white md:text-lg font-light mb-3'}>OVERVIEW</Text>
          <div
            className="relative grid grid-cols-2 md:grid-cols-2 md:gap-24 gap-12"
            style={{
              zIndex: 2
            }}
          >
            <div>
              {this.renderTitle()}
            </div>
            <div
              style={{
                minHeight: 400
              }}
            >
              <div style={styles.backdrop}>
                {this.renderDesc()}
              </div>
            </div>
          </div>
          <div
            className="absolute bottom-0 z-10 pointer-events-none left-0"
            style={{
              zIndex: 0,
              bottom: 0,
              transform: 'translate(0px, 46%)'
            }}
          >
            <Image layout={'intrinsic'} objectFit={'fill'} src={mountain}/>
          </div>
        </div>
      </div>
    );
  }
};

const styles = {
  tiltBox: {
    background: 'none',
    borderRadius: 32,
  },
  background: {
    borderRadius: 32,
    background: 'linear-gradient(159.63deg, rgba(201, 226, 124, 0.7) 6.66%, rgba(136, 181, 0, 0.7) 86.46%)',
  },
  backdrop: {
    padding: '40px 60px 40px 60px',
    borderRadius: 32,
    border: '3px solid rgba(255, 255, 255, 0.3)',
    backdropFilter: 'blur(15px)',
    background: 'linear-gradient(110.62deg, rgba(255, 255, 255, 0.25) 6.39%, rgba(255, 255, 255, 0.065) 53.34%, rgba(255, 255, 255, 0.1025) 53.34%)',
  }
}
