import React, {Fragment} from "react";
import Image, {ImageProps} from 'next/image';

import 'react-awesome-slider/dist/styles.css';
import 'react-awesome-slider/dist/custom-animations/scale-out-animation.css';
import Text, {TextProps} from "@components/typography";
import {colors} from "@definitions/colors";
import bgOverviewBottom from "@public/images/bgOverviewBottom.png";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/pagination";
import { Pagination } from "swiper";

export interface CarouselItem {
  title?: string;
  link?: string;
  image: ImageProps['src'] | string;
}

export interface CarouselProps {
  label?: string;
  title?: string;
  description?: string;
  items: Array<CarouselItem>
}

export interface State {
  activeSlide: number;
}

export default class HeroCarousel extends React.Component<CarouselProps, State> {
  parseText = (text: string) => {
    return text.replace(/\[br\]/g, '<br/>')
      .replace(/\[hl\]/g,`<span style="color: ${colors.primary.main}">`)
      .replace(/\[\/hl\]/g, '</span>');
  }

  clearText = (text: string) => {
    return text.replace(/\[br\]/g, '')
      .replace(/\[hl\]/g,``)
      .replace(/\[\/hl\]/g, '');
  }

  renderTitle = () => {
    const {title} = this.props;
    if (!title) {
      return null;
    }
    return (
      <Text asDiv={true} className={'lg:text-5xl sm:text-3xl text-2xl lg:mb-8 mb-3'} shade={'dark'}>
        <div style={{lineHeight: 1.2}} dangerouslySetInnerHTML={{ __html: this.parseText(title)}}/>
      </Text>
    );
  }

  renderDesc = () => {
    const {description} = this.props;
    if (!description) {
      return null;
    }
    return (
      <Text asDiv={true} type={'normal'} className={'md:text-lg text-md md:text-lg'}>
        <div className="hidden md:block" style={{lineHeight: 1.3}} dangerouslySetInnerHTML={{ __html: this.parseText(description)}}/>
        <span className="md:hidden">{this.clearText(description)}</span>
      </Text>
    );
  }

  renderItems = () => {
    const {items} = this.props;
    const elems: Array<React.ReactElement> = [];
    items.map((item, index) => {
      elems.push(
        <SwiperSlide key={index} style={{width: 420, height: 320}}>
          <a href={item.link}
            className="group"
             style={{
               display: 'block',
               position: 'relative',
               width: 400,
               height: 300,
               boxShadow: '-30px 30px rgba(173, 255, 0, 0.2)'
             }}
          >
            <Image
              layout={'fill'}
              objectFit={'cover'}
              src={item.image}
              unselectable={'on'}
            />
          </a>
        </SwiperSlide>
      );
    });

    return (
      <Swiper
        slidesPerView={'auto'}
        centeredSlides={true}
        spaceBetween={60}
        loop={true}
        pagination={{
          clickable: true,
        }}
        modules={[Pagination]}
        className="mySwiper"
        style={{
          height: 320
        }}
      >
        {elems}
      </Swiper>
    );
  }

  render() {
    const {label} = this.props;
    return (
      <div
        className="relative items-center pb-32 lg:pb-20"
      >
        <div className="container h-auto px-10 md:px-5 lg:px-20 relative mx-auto py-20">
          {!!label &&
          <Text asDiv={true} color={colors.primary.main} className={'text-xs md:text-lg font-light mb-1'}>
            {label}
          </Text>
          }
          {this.renderTitle()}
          {this.renderDesc()}
        </div>
        {this.renderItems()}
      </div>
    );
  }


};
