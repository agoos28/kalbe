import React, {Fragment} from "react";
import Image, {ImageProps} from 'next/image';

import 'react-awesome-slider/dist/styles.css';
import 'react-awesome-slider/dist/custom-animations/scale-out-animation.css';
import Text, {TextProps} from "@components/typography";
import {colors} from "@definitions/colors";
import exploreBgBottom from "@public/images/exploreBgBottom.png";


export interface NewsItem {
  title: string;
  link?: string;
  image: ImageProps['src'];
}

export interface NewsProps {
  items: Array<NewsItem>
}

export interface State {
  activeSlide: number;
}

export default class HeroNews extends React.Component<NewsProps, State> {
  renderItems = () => {
    const {items} = this.props;
    const elems: Array<React.ReactElement> = [];
    items.map((item, index) => {
      elems.push(
        <div>
          <a
            key={index} href={item.link}
            className="group relative w-full"
          >
            <div
              className="mb-10 rounded-xl overflow-hidden"
              style={{
                boxShadow: '-30px 25px rgba(173, 255, 0, 0.2)'
              }}
            >
              <Image layout={'intrinsic'} src={item.image}/>
            </div>
            <Text>{item.title}</Text>
          </a>
        </div>
      );
    });

    return (
      <div className="grid grid-cols-3 gap-12 pt-10">
        {elems}
      </div>
    );
  }

  render() {
    return (
      <div
        className="relative flex items-center pb-32 lg:pb-20"
        style={{
          backgroundImage: 'url(/images/newsBg.jpg)',
          backgroundPosition: '20% 50%',
          backgroundRepeat: 'no-repeat',
          backgroundSize: 'cover'
        }}
      >
        <div className="container h-auto px-10 lg:px-20 relative mx-auto py-20">
          <Text asDiv={true} color={colors.primary.main} className={'text-xs text-center md:text-lg font-light mb-1'}>LATEST NEWS</Text>
          <Text asDiv={true} className={'text-center lg:text-5xl sm:text-3xl text-2xl lg:mb-8 mb-3'} shade={'dark'}>
            SEE WHAT WE’VE BEEN UP TO
          </Text>
          {this.renderItems()}
        </div>

      </div>
    );
  }


};
