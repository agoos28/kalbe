import React, {Fragment} from "react";
import Image, {ImageProps} from 'next/image';
import relation from '@public/images/relation.png';
import 'react-awesome-slider/dist/styles.css';
import 'react-awesome-slider/dist/custom-animations/scale-out-animation.css';
import Text, {TextProps} from "@components/typography";
import {colors} from "@definitions/colors";
import bgOverviewBottom from "@public/images/bgOverviewBottom.png";
// @ts-ignore
import Tilt from 'react-vanilla-tilt';




export interface MapProps {
  query: string
}

export default class HeroMap extends React.Component<MapProps> {

  render() {
    return (
      <div className="mapouter">
        <div className="gmap_canvas">
          <iframe width="100%" height="500" id="gmap_canvas"
                  src="https://maps.google.com/maps?q=Kalbe%20Farma&t=&z=15&ie=UTF8&iwloc=&output=embed" frameBorder="0"
                  scrolling="no" />
        </div>
      </div>
    );
  }
};

const styles = {
  tiltBox: {
    background: 'none',
    borderRadius: 32,
  },
  background: {
    borderRadius: 32,
    background: 'linear-gradient(159.63deg, rgba(201, 226, 124, 0.7) 6.66%, rgba(136, 181, 0, 0.7) 86.46%)',
  },
  backdrop: {
    padding: '40px 60px 40px 60px',
    borderRadius: 32,
    border: '3px solid rgba(255, 255, 255, 0.3)',
    backdropFilter: 'blur(15px)',
    background: 'linear-gradient(110.62deg, rgba(255, 255, 255, 0.25) 6.39%, rgba(255, 255, 255, 0.065) 53.34%, rgba(255, 255, 255, 0.1025) 53.34%)',
  }
}
