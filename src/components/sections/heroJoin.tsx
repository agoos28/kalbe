import React, {Fragment} from "react";
import Image, {ImageProps} from 'next/image';

import 'react-awesome-slider/dist/styles.css';
import 'react-awesome-slider/dist/custom-animations/scale-out-animation.css';
import Text, {TextProps} from "@components/typography";
import {colors} from "@definitions/colors";
import exploreBgBottom from "@public/images/exploreBgBottom.png";


export interface JoinItem {
  title: string;
  description?: string;
  link?: string;
  image: ImageProps['src'] | string;
}

export interface JoinProps {
  title: string;
  description?: string;
  items: Array<JoinItem>
}

export interface State {
  activeSlide: number;
}

export default class HeroJoin extends React.Component<JoinProps, State> {
  parseText = (text: string) => {
    return text.replace(/\[br\]/g, '<br/>')
      .replace(/\[hl\]/g,`<span style="color: ${colors.primary.main}">`)
      .replace(/\[\/hl\]/g, '</span>');
  }

  clearText = (text: string) => {
    return text.replace(/\[br\]/g, '')
      .replace(/\[hl\]/g,``)
      .replace(/\[\/hl\]/g, '');
  }

  renderTitle = () => {
    const {title} = this.props;
    return (
      <Text asDiv={true} className={'text-center lg:text-5xl sm:text-3xl text-2xl lg:mb-8 mb-3'} shade={'dark'}>
        <div style={{lineHeight: 1.2}} dangerouslySetInnerHTML={{ __html: this.parseText(title)}}/>
      </Text>
    );
  }

  renderDesc = () => {
    const {description} = this.props;
    if (!description) {
      return null;
    }
    return (
      <Text asDiv={true} type={'normal'} className={'text-center md:text-lg text-sm px-5'}>
        <div className="hidden md:block" style={{lineHeight: 1.3}} dangerouslySetInnerHTML={{ __html: this.parseText(description)}}/>
        <span className="md:hidden">{this.clearText(description)}</span>
      </Text>
    );
  }

  renderItems = () => {
    const {items} = this.props;
    const elems: Array<React.ReactElement> = [];
    items.map((item, index) => {
      elems.push(
          <a
            key={index} href={item.link}
            className="group block relative w-full"
            style={styles.backdrop}
          >
            <div>
              <Image layout={'responsive'} height={100} width={'100%'} src={item.image}/>
            </div>
            <Text asDiv={true} type={'large'} className="text-center">{item.title}</Text>
            <Text>{item.description}</Text>
          </a>
      );
    });

    return (
      <div className="grid grid-cols-1 md:grid-cols-2 gap-10 pt-10">
        {elems}
      </div>
    );
  }

  render() {
    return (
      <div
        className="relative flex items-center pb-32 lg:pb-20"
        style={{
          backgroundImage: 'url(/images/joinUsBg.jpg)',
          backgroundPosition: '20% 50%',
          backgroundRepeat: 'no-repeat',
          backgroundSize: 'cover'
        }}
      >
        <div className="container max-w-screen-lg h-auto px-10 lg:px-20 relative mx-auto py-20">
          <Text asDiv={true} color={colors.primary.main} className={'text-center text-xs md:text-lg font-light mb-1'}>JOIN US</Text>
          {this.renderTitle()}
          {this.renderDesc()}
          {this.renderItems()}
        </div>

      </div>
    );
  }
};

const styles = {
  tiltBox: {
    background: 'none',
    borderRadius: 32,
  },
  background: {
    borderRadius: 32,
    background: 'linear-gradient(159.63deg, rgba(201, 226, 124, 0.7) 6.66%, rgba(136, 181, 0, 0.7) 86.46%)',
  },
  backdrop: {
    padding: '40px 60px 40px 60px',
    borderRadius: 32,
    border: '3px solid rgba(255, 255, 255, 0.3)',
    backdropFilter: 'blur(15px)',
    background: 'linear-gradient(110.62deg, rgba(255, 255, 255, 0.25) 6.39%, rgba(255, 255, 255, 0.065) 53.34%, rgba(255, 255, 255, 0.1025) 53.34%)',
  }
}
