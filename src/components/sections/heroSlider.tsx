import React, {Fragment} from "react";
import {useTranslation} from "react-i18next";
import Image, {ImageProps} from 'next/image';
import slide1 from '@public/images/homeSlide1.jpg';
import AwesomeSlider from 'react-awesome-slider';

import {Button} from "@components";
import Text, {TextProps} from "@components/typography";
import {colors} from "@definitions/colors";
import {Utils} from "../../utils";

export interface SliderItem {
  title: string;
  description?: string;
  link?: string;
  backgroundImage?: ImageProps['src'];
  image?: ImageProps['src'];
}

export interface SliderProps {
  content: Array<SliderItem>
}

export interface State {
  activeSlide: number;
}

export default class HeroSlider extends React.Component<SliderProps, State> {
  render() {
    const {content} = this.props;
    if (!content) {
      return null;
    }
    return (
      <div className="relative h-[100vh] sm:h-[50vh] md:h-[60vh] lg:h-[100vh] min-h-[800px]">
        <AwesomeSlider
          animation="scaleOutAnimation"
          organicArrows={false}
          fillParent={true}
          bullets={false}
        >
          {content.map((item, index) =>
            <div key={index} className="bg-black">
              <div className="absolute w-full z-10 left-0 top-0 bottom-0">
                <div className="container h-full mx-auto px-10 sm:px-6 sm:flex items-center">
                  <div className="sm:w-7/12 pr-5 sm:mt-0 md:mt-20 mt-36 lg:mt-0">
                    <h2 className={'mb-0 lg:mb-3'}>
                      <Text className="lg:text-5xl sm:text-3xl text-2xl" color={colors.textWhite.main}>{item.title}</Text>
                    </h2>
                    {!!item.description &&
                    <h3 className={'mb-5'}>
                      <Text className="lg:text-lg text-md" color={colors.primary.main}>{item.description}</Text>
                    </h3>
                    }
                    {!!item.link &&
                    <Button href={'#'} className="rounded-full hover:opacity-70 transition-all">Learn more</Button>
                    }
                  </div>
                  {!!item.image &&
                  <div className="sm:w-5/12 sm:mx-0 lg:w-4/12 mx-auto w-9/12 pt-20">
                    <Image src={item.image} />
                  </div>
                  }
                </div>
              </div>
              <Image color={colors.paper.main} layout={'fill'} objectFit={'cover'} objectPosition={'left'} draggable={false} src={item.backgroundImage || slide1} />
            </div>
          )}
        </AwesomeSlider>
        <div className="hidden lg:block absolute w-full z-10" style={{bottom: 180}}>
          <div className="container mx-auto px-6 relative">
            <div className="absolute">
              <div className="absolute rounded-xl bg-primary top-0 left-0 right-0 bottom-0" style={styles.stockBg}/>
              <div className="rounded-xl py-5 px-9 relative backdrop-blur-xl" style={styles.stock}>
                <div className="flex items-center">
                  <div className="w-5/12 mr-3">
                    <div><Text className="text-lg lg:text-3xl" color={colors.textWhite.main}>KLBE</Text></div>
                    <div><Text className="text-xs lg:text-xs" color={colors.textWhite.main} style={{letterSpacing: 1, fontWeight: 300}}>
                      {Utils.dateTimeFormatter(Date(), 'DD / MM / YY hh:mm') + ' WIB'}
                    </Text></div>
                  </div>
                  <div className="w-7/12">
                    <div><Text className="lg:text-5xl text-4xl" color={colors.textWhite.main}>1.690</Text></div>
                    <Text className="lg:text-sm text-xs" color={colors.textWhite.main} style={{letterSpacing: 1}}>+12.00 (+0.93%)</Text>
                  </div>

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
};

const styles = {
  stockBg: {
    background: 'linear-gradient(180deg, rgba(141, 187, 0, 0.7) 0%, rgba(131, 173, 1, 0.7) 100%)'
  },
  stock: {
    transform: 'translate(30px, -30px)',
    background: 'linear-gradient(0deg, rgba(173, 255, 0, 0.2), rgba(173, 255, 0, 0.2)), linear-gradient(110.62deg, rgba(255, 255, 255, 0.25) 6.39%, rgba(255, 255, 255, 0) 53.34%)'
  }
}
