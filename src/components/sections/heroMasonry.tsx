import React, {Fragment} from "react";
import Image, {ImageProps} from 'next/image';

import 'react-awesome-slider/dist/styles.css';
import 'react-awesome-slider/dist/custom-animations/scale-out-animation.css';
import Text, {TextProps} from "@components/typography";
import {colors} from "@definitions/colors";
import exploreBgBottom from "@public/images/exploreBgBottom.png";


export interface MasonryItem {
  title: string;
  link?: string;
  image: ImageProps['src'];
  display?: 'wide' | 'tall' | 'big'
}

export interface MasonryProps {
  title: string;
  description?: string;
  items: Array<MasonryItem>
}

export interface State {
  items: Array<MasonryItem>;
}

export default class HeroMasonry extends React.Component<MasonryProps, State> {
  constructor(props: MasonryProps) {
    super(props);
    this.state = {
      items: props.items
    }
  }
  parseText = (text: string) => {
    return text.replace(/\[br\]/g, '<br/>')
      .replace(/\[hl\]/g,`<span style="color: ${colors.primary.main}">`)
      .replace(/\[\/hl\]/g, '</span>');
  }

  clearText = (text: string) => {
    return text.replace(/\[br\]/g, '')
      .replace(/\[hl\]/g,``)
      .replace(/\[\/hl\]/g, '');
  }

  renderTitle = () => {
    const {title} = this.props;
    return (
      <Text asDiv={true} className={'lg:text-5xl sm:text-3xl text-2xl lg:mb-8 mb-3'} shade={'dark'}>
        <div style={{lineHeight: 1.2}} dangerouslySetInnerHTML={{ __html: this.parseText(title)}}/>
      </Text>
    );
  }

  renderDesc = () => {
    const {description} = this.props;
    if (!description) {
      return null;
    }
    return (
      <Text asDiv={true} type={'normal'} className={'md:text-lg text-sm mb-16'}>
        <div className="hidden md:block" style={{lineHeight: 1.3}} dangerouslySetInnerHTML={{ __html: this.parseText(description)}}/>
        <span className="md:hidden">{this.clearText(description)}</span>
      </Text>
    );
  }

  updateImageSize = (size: {naturalWidth: number; naturalHeight: number; }, index: number) => {
    const {items} = this.state;
    if (size.naturalHeight === 1 && size.naturalWidth === 1) {
      return;
    }
    const ratio = size.naturalWidth / size.naturalHeight;
    let display = 'big';
    if (ratio < 0.7) {
      display = 'tall'
    }
    if (ratio > 1.4) {
      display = 'wide'
    }
    console.log(size, ratio, display);
    const _items = [...items];
    _items[index].display = display as any;
    this.setState({
      items: _items
    })
  }

  renderItems = () => {
    const {items} = this.state;
    const elems: Array<React.ReactElement> = [];
    const sizes = ['normal', 'normal', 'big', 'normal', 'wide', 'tall', 'normal'];
    items.map((item, index) => {
      elems.push(
        <a
          key={index} href={item.link}
          className={'group ' + (item.display || sizes[Math.floor(Math.random() * 6)])}
        >
          <Image
            layout={'fill'}
            src={item.image}
            onLoadingComplete={(size) => !item.display && this.updateImageSize(size, index)}
          />
        </a>
      );
    });

    return (
      <div className="grid-wrapper">
        {elems}
      </div>
    );
  }

  render() {
    return (
      <div
        className="relative flex items-center pb-32 lg:pb-20"
      >
        <div className="container flex flex-col h-auto px-10 lg:px-20 relative mx-auto py-20">
          <Text asDiv={true} color={colors.primary.main} className={'text-xs md:text-lg font-light mb-3'}>EXPLORE</Text>
          {this.renderTitle()}
          {this.renderDesc()}
          {this.renderItems()}
        </div>
      </div>
    );
  }


};
