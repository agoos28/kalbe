import React, {Fragment} from "react";
import Image, {ImageProps} from 'next/image';
import innovationImg from '@public/images/innovationImg.png';
import 'react-awesome-slider/dist/styles.css';
import 'react-awesome-slider/dist/custom-animations/scale-out-animation.css';
import Text, {TextProps} from "@components/typography";
import {colors} from "@definitions/colors";
import bgOverviewBottom from "@public/images/bgOverviewBottom.png";
// @ts-ignore
import Tilt from 'react-vanilla-tilt';




export interface InnovationProps {
  title: string;
  description: string;
  link: string;
}

export default class HeroInnovation extends React.Component<InnovationProps> {
  parseText = (text: string) => {
    return text.replace(/\[br\]/g, '<br/>')
      .replace(/\[hl\]/g,`<span style="color: ${colors.primary.main}">`)
      .replace(/\[\/hl\]/g, '</span>');
  }

  clearText = (text: string) => {
    return text.replace(/\[br\]/g, '')
      .replace(/\[hl\]/g,``)
      .replace(/\[\/hl\]/g, '');
  }

  renderTitle = () => {
    const {title} = this.props;
    return (
      <Text asDiv={true} className={'lg:text-5xl sm:text-3xl text-2xl lg:mb-8 mb-3'} color={colors.textWhite.main}>
        <div style={{lineHeight: 1.2}} dangerouslySetInnerHTML={{ __html: this.parseText(title)}}/>
      </Text>
    );
  }

  renderDesc = () => {
    const {description} = this.props;
    return (
      <Text asDiv={true} type={'normal'} className={'md:text-lg text-md md:text-lg'} color={colors.textWhite.main}>
        <div className="hidden md:block" style={{lineHeight: 1.3}} dangerouslySetInnerHTML={{ __html: this.parseText(description)}}/>
        <span className="md:hidden">{this.clearText(description)}</span>
      </Text>
    );
  }



  render() {
    return (
      <div
        className="relative flex items-center justify-center"
        style={{
          padding: '80px 0px 80px 0px',
          backgroundImage: 'url(/images/innovation.png)',
          backgroundPosition: '50% 50%',
          backgroundSize: '100% 100%',
          backgroundRepeat: 'no-repeat',
        }}
      >
        <div className="container h-auto px-10 md:px-5 lg:px-20 relative mx-auto py-20">
          <div
            className="relative"
            style={{
              zIndex: 2
            }}
          >
            <div>
              <div style={styles.backdrop} className="grid grid-cols-2">
                <div>
                  {this.renderTitle()}
                  {this.renderDesc()}
                </div>
                <div>
                  <div
                    style={{
                      transform: 'translate(0%, -30%)'
                    }}
                  >
                    <Image layout={'intrinsic'} objectFit={'fill'} src={innovationImg}/>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div
            className="absolute bottom-0 z-10 pointer-events-none"
            style={{
              zIndex: 0,
              top: -80,
              right: 40
            }}
          >
            <Image layout={'intrinsic'} objectFit={'fill'} src={innovationImg}/>
          </div>
        </div>
      </div>
    );
  }
};

const styles = {
  tiltBox: {
    background: 'none',
    borderRadius: 32,
  },
  background: {
    borderRadius: 32,
    background: 'linear-gradient(159.63deg, rgba(201, 226, 124, 0.7) 6.66%, rgba(136, 181, 0, 0.7) 86.46%)',
  },
  backdrop: {
    padding: '40px 60px 40px 60px',
    borderRadius: 32,
    border: '3px solid rgba(255, 255, 255, 0.3)',
    backdropFilter: 'blur(15px)',
    background: 'linear-gradient(110.62deg, rgba(255, 255, 255, 0.25) 6.39%, rgba(255, 255, 255, 0.065) 53.34%, rgba(255, 255, 255, 0.1025) 53.34%)',
  }
}
