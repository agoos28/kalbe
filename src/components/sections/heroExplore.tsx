import React, {Fragment} from "react";
import Image, {ImageProps} from 'next/image';

import 'react-awesome-slider/dist/styles.css';
import 'react-awesome-slider/dist/custom-animations/scale-out-animation.css';
import Text, {TextProps} from "@components/typography";
import {colors} from "@definitions/colors";
import exploreBgBottom from "@public/images/exploreBgBottom.png";


export interface ExploreItem {
  title: string;
  link?: string;
  image: ImageProps['src'];
}

export interface ExploreProps {
  title: string;
  description?: string;
  items: Array<ExploreItem>
}

export interface State {
  activeSlide: number;
}

export default class HeroExplore extends React.Component<ExploreProps, State> {
  parseText = (text: string) => {
    return text.replace(/\[br\]/g, '<br/>')
      .replace(/\[hl\]/g,`<span style="color: ${colors.primary.main}">`)
      .replace(/\[\/hl\]/g, '</span>');
  }

  clearText = (text: string) => {
    return text.replace(/\[br\]/g, '')
      .replace(/\[hl\]/g,``)
      .replace(/\[\/hl\]/g, '');
  }

  renderTitle = () => {
    const {title} = this.props;
    return (
      <Text asDiv={true} className={'lg:text-5xl sm:text-3xl text-2xl lg:mb-8 mb-3'} shade={'dark'}>
        <div style={{lineHeight: 1.2}} dangerouslySetInnerHTML={{ __html: this.parseText(title)}}/>
      </Text>
    );
  }

  renderDesc = () => {
    const {description} = this.props;
    if (!description) {
      return null;
    }
    return (
      <Text asDiv={true} type={'normal'} className={'md:text-lg text-sm px-5'}>
        <div className="hidden md:block" style={{lineHeight: 1.3}} dangerouslySetInnerHTML={{ __html: this.parseText(description)}}/>
        <span className="md:hidden">{this.clearText(description)}</span>
      </Text>
    );
  }

  renderItems = () => {
    const {items} = this.props;
    const elems: Array<React.ReactElement> = [];
    items.map((item, index) => {
      elems.push(
        <div>
          <a
            key={index} href={item.link}
            className="group relative w-full"
          >
            <Image layout={'intrinsic'} src={item.image}/>
          </a>
        </div>
      );
    });

    return (
      <div className="grid grid-cols-2 gap-10 pt-10">
        {elems}
      </div>
    );
  }

  render() {
    return (
      <div
        className="relative flex items-center pb-32 lg:pb-20"
        style={{
          backgroundImage: 'url(/images/exploreBg.jpg)',
          backgroundPosition: '20% 50%',
          backgroundRepeat: 'no-repeat',
          backgroundSize: 'cover'
        }}
      >
        <div className="container h-auto px-10 lg:px-20 relative mx-auto py-20">
          <Text asDiv={true} color={colors.primary.main} className={'text-xs md:text-lg font-light mb-1'}>EXPLORE</Text>
          {this.renderTitle()}
          {this.renderDesc()}
          {this.renderItems()}
        </div>

      </div>
    );
  }


};
