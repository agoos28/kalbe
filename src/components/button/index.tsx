import React from "react";
import {colors} from "@definitions/colors";

export type IButton = React.DetailedHTMLProps<
  React.ButtonHTMLAttributes<HTMLButtonElement>,
  HTMLButtonElement
> & React.DetailedHTMLProps<React.AnchorHTMLAttributes<HTMLAnchorElement>, HTMLAnchorElement>;

export const Button: React.FC<IButton> = ({
  className = "",
  href,
  children,
  ...rest
}) => {
  if (href) {
    return (
      <a
        className={`py-2 px-8 rounded text-textWhite-main text-sm ${className}`}
        {...rest}
        href={href}
        style={{
          ...rest.style,
          backgroundColor: colors.primary.main
        }}
      >
        {children}
      </a>
    );
  } 
  return (
    <button
      className={`py-1 px-8 rounded text-textWhite-main text-xs ${className}`}
      {...rest}
      style={{
        ...rest.style,
        backgroundColor: colors.primary.main
      }}
    >
      {children}
    </button>
  );
};
