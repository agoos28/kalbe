import React from "react";
import {SimplePaletteColorOptions, SpacingType, ThemeFont} from "../../models/theme";
import {colors} from "@definitions/colors";


export interface TextProps extends React.HTMLAttributes<React.ReactHTML['span']>{
  asDiv?: boolean;
  type?: keyof ThemeFont['style'];
  weight?: keyof ThemeFont['family'];
  align?: 'auto' | 'left' | 'right' | 'center' | 'justify';
  color?: string;
  shade?: keyof SimplePaletteColorOptions;
  selectable?: boolean;
  numberOfLines?: number;
  ellipsizeMode?: 'head' | 'middle' | 'tail' | 'clip';
  bottomSpacing?: SpacingType;
}

export default class Text extends React.PureComponent<TextProps> {
  defineClassName = () => {
    const {className, type} = this.props;

    const classes = className?.split(' ') || [];

    switch (type) {
      case "headline":
        classes.push('text-5xl')
        break;
      case "large":
        classes.push('text-lg')
        break;
      case "small":
        classes.push('text-sm')
        break;
      case "tiny":
        classes.push('text-xs')
        break;
    }

    return classes.join(' ');
  }

  defineStyle = () => {
    const {color, shade, style, asDiv} = this.props;

    const _style: Partial<CSSStyleDeclaration> = {
      color: colors.textBlack[shade || 'main']
    };

    _style.display = asDiv ? 'block' : 'inline-block'

    if (color) {
      _style.color = color
    }

    return {...style, ..._style};
  }

  render() {
    const _style = this.defineStyle();
    const _className = this.defineClassName();
    if (this.props.asDiv) {
      return (
        <div style={_style as any} className={_className}>{this.props.children}</div>
      );
    }
    return (
      <span style={_style as any} className={_className}>{this.props.children}</span>
    );
  }
}

const styles: Record<string, CSSStyleDeclaration> = {

}