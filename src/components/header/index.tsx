import React from "react";

import { Logo } from "@components/index.js";
import {MainHeader} from "@components/header/mainHeader";

export const Header: React.FC = () => {
  return (
    <MainHeader />
  );
};
