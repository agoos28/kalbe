import React, {Fragment} from "react";
import {Popover, Transition} from "@headlessui/react";
import {MenuIcon, XIcon, SearchIcon} from "@heroicons/react/solid";
import {useTranslation} from "react-i18next";
import Image from 'next/image';
import logo from '@public/images/logo.png'
import LangSelection from "@components/button/langSelection";
import {useRouter} from "next/router";

export const MainHeader: React.FC = () => {
  const { t, i18n } = useTranslation('common');
  const navigation = [
    { name: t('home'), href: '/' },
    { name: t('aboutUs'), href: '/aboutUs' },
    { name: t('sustainability'), href: '/sustainability' },
    { name: t('productService'), href: '/productService' },
    { name: t('news'), href: '/news' },
    { name: t('investors'), href: '/investors' },
    { name: t('contact'), href: '/contact' },
  ];
  const router = useRouter()
  return (
    <Popover>
      <div className="md:absolute fixed right-0 left-0 top-6 z-10">
        <div className="absolute rounded-full -translate-y-2/4 -translate-x-1/4" style={styles.logoBg} />
        <div className="container mx-auto px-6">
          <nav className="flex items-center">
            <div className="flex items-center flex-grow flex-shrink-0 md:flex-grow-0">
              <div className="flex items-center justify-between w-full md:w-auto">
                <a href="#">
                  <span className="sr-only">logo</span>
                  <Image className={'h-auto'} width={131} src={logo}/>
                </a>
                <div className="-mr-2 flex items-center md:hidden">
                  <Popover.Button className="bg-white rounded-md p-2 inline-flex items-center justify-center text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-gray-300">
                    <span className="sr-only">Open main menu</span>
                    <MenuIcon className="h-6 w-6" aria-hidden="true" />
                  </Popover.Button>
                </div>
              </div>
            </div>
            <div className="hidden md:flex lg:ml-20 md:ml-10 md:pr-4 mt-6 flex-1 flex-wrap">
              {navigation.map((item) => (
                <a
                  key={item.name}
                  href={item.href}
                  className={(item.href === router.route ? 'font-bold' : 'font-normal') + ' py-2 text-sm text-white hover:text-gray-300 md:mr-4 lg:mr-8 whitespace-nowrap transition-all'}
                >
                  {item.name}
                </a>
              ))}
            </div>
            <div className="hidden md:block">
              <div className="hidden md:flex items-center mt-6 ">
                <a className="font-medium text-sm text-white hover:text-gray-300 mr-6">
                  <SearchIcon className="h-4 w-4" />
                </a>
                <LangSelection />
              </div>
            </div>
          </nav>
        </div>
      </div>

      <Transition
        as={Fragment}
        enter="duration-150 ease-out"
        enterFrom="opacity-0 scale-95"
        enterTo="opacity-100 scale-100"
        leave="duration-100 ease-in"
        leaveFrom="opacity-100 scale-100"
        leaveTo="opacity-0 scale-95"
      >
        <Popover.Panel
          focus
          className="absolute z-20 top-0 inset-x-0 p-2 transition transform origin-top-right md:hidden"
        >
          <div className="rounded-lg shadow-md bg-white ring-1 ring-black ring-opacity-5 overflow-hidden">
            <div className="px-5 pt-4 flex items-center justify-between">
              <div>
                <Image className={'h-auto'} width={131} src={logo}/>
              </div>
              <div className="-mr-2">
                <Popover.Button className="bg-white rounded-md p-2 inline-flex items-center justify-center text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-gray-300">
                  <span className="sr-only">Close main menu</span>
                  <XIcon className="h-6 w-6" aria-hidden="true" />
                </Popover.Button>
              </div>
            </div>
            <div className="px-2 pt-2 pb-3 space-y-1">
              {navigation.map((item) => (
                <a
                  key={item.name}
                  href={item.href}
                  className={(item.href === router.route ? 'font-bold' : 'font-normal') + ' block px-3 py-2 rounded-md text-base text-gray-700 hover:text-gray-900 hover:bg-gray-50'}
                >
                  {item.name}
                </a>
              ))}
            </div>
          </div>
        </Popover.Panel>
      </Transition>
    </Popover>
  );
};

const styles = {
  logoBg: {
    top: -45,
    height: 400,
    width: 400,
    background: 'linear-gradient(228.46deg, rgba(255, 255, 255, 0.3) 9.24%, rgba(255, 255, 255, 0) 96.17%)'
  }
}
