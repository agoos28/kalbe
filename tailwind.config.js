const colors = require('tailwindcss/colors')

module.exports = {
  purge: ["./pages/**/*.tsx", "./src/**/*.tsx"],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      fontFamily: {
        "helvetica": ["helvetica"],
      },
    },
    colors: {
      ...colors,
      primary: {light: '#ABCA4B', main: '#8DBB00', dark: '#8DBB00'},
      textBlack: {lighter: '#F1F1F1', light: '#A5A6AA', main: '#585858', dark: '#000000'},
      textWhite: {light: '#ffffff', main: '#ffffff', dark: '#ffffff7d'},
    }
  },
  variants: {
    extend: {},
  },
  content: ['./src/**/*.{html,js}', './node_modules/tw-elements/dist/js/**/*.js'],
  plugins: [
    require('tw-elements/dist/plugin')
  ]
};
